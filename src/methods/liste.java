
package methods;

public class liste {
   
    public final int id, tutar;
    public final String bankaAdi, hesapNo, cekNo, vade, alici, uyariTarihi, odenmeDurum;

    public liste(int id, String bankaAdi, String hesapNo, String cekNo, int tutar, String vade, String alici, String uyariTarihi, String odenmeDurum){
        this.id = id;
        this.bankaAdi = bankaAdi;
        this.hesapNo = hesapNo;
        this.cekNo = cekNo;
        this.tutar = tutar;
        this.vade = vade;
        this.alici = alici;
        this.uyariTarihi = uyariTarihi;
        this.odenmeDurum = odenmeDurum;
        
    }

   public int getId(){
   return id;
   }
   
   public int gettutar(){
   return tutar;
   }
   
    public String getbankaAdi(){
   return bankaAdi;
   }
    
    public String gethesapNo(){
   return hesapNo;
   }
   
    
    public String getcekNo(){
   return cekNo;
   }
    
    public String getvade(){
   return vade;
   }
    
    public String getalici(){
   return alici;
    }
    
     public String getuyariTarihi(){
   return uyariTarihi;
    }
     
     public String getodenmeDurum(){
   return odenmeDurum;
    }

   
    
    @Override
    public String toString(){
        return "Id: "+id+" Banka: "+bankaAdi+" Hesap No: "+hesapNo+
                " Çek No: "+cekNo+" Tutar: "+tutar+" Vade: "+vade+
                " Alıcı: "+alici+" Uyarı Tarihi: "+uyariTarihi+" Ödenme Durumu: "+odenmeDurum;
    }
}
