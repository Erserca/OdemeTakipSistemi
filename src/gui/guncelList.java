
package gui;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UnsupportedLookAndFeelException;
import methods.tableGuncelleme;

public class guncelList extends javax.swing.JFrame {

    
     static String vade, uyariVade, odenme;
     static int id;
     tableGuncelleme jts = new tableGuncelleme();
     
    public guncelList() {
        initComponents();
        jts.dbListeleParametre();
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        container = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        vtTablo = new javax.swing.JTable();
        alarmlbl = new javax.swing.JLabel();
        bg_lbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        container.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.setOpaque(false);
        jScrollPane1.setPreferredSize(new java.awt.Dimension(750, 403));

        vtTablo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "id", "Banka", "Hesap No", "Çek No", "Tutar", "Vade", "Alıcı", "Uyarı Tarihi", "Durum"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        vtTablo.setOpaque(false);
        vtTablo.setPreferredSize(new java.awt.Dimension(750, 64));
        jScrollPane1.setViewportView(vtTablo);

        container.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 120, 750, 150));

        alarmlbl.setText("Ödeme uyarısı: Günlük ödemesi gelen belge listeniz aşağıdadır");
        container.add(alarmlbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 460, 20));
        container.add(bg_lbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 270));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(container, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(container, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        container.getAccessibleContext().setAccessibleName("");

        setSize(new java.awt.Dimension(776, 313));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents


    public static void main(String args[]) {
        
        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
if ("Windows".equals(info.getName())) {
try {
javax.swing.UIManager.setLookAndFeel(info.getClassName());
} catch (ClassNotFoundException ex) {
Logger.getLogger(belgeGiris.class.getName()).log(Level.SEVERE, null, ex);
} catch (InstantiationException ex) {
                       Logger.getLogger(belgeGiris.class.getName()).log(Level.SEVERE, null, ex);
} catch (IllegalAccessException ex) {
                      Logger.getLogger(belgeGiris.class.getName()).log(Level.SEVERE, null, ex);
                   } catch (UnsupportedLookAndFeelException ex) {
                       Logger.getLogger(belgeGiris.class.getName()).log(Level.SEVERE, null, ex);
                  }
                  break;
            }
}   

        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new guncelList().setVisible(true);
            }
        });
    
       
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel alarmlbl;
    private javax.swing.JLabel bg_lbl;
    private javax.swing.JPanel container;
    public static javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable vtTablo;
    // End of variables declaration//GEN-END:variables
}
