
package methods;

import gui.belgeGiris;
import gui.guncelList;
import java.awt.AWTException;
import java.awt.Font;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import methods.conf;

public class trayIcon {
     public static TrayIcon tIcon;
    
    public trayIcon(){
    
        showTrayIcon();
    }
    
    public static void showTrayIcon()
    {
        if (!SystemTray.isSupported()) {
            JOptionPane.showMessageDialog(null,"Sistem, Tray Icon eklentisini desteklemiyor!");
            System.exit(0);
            return;
        }
        
        final PopupMenu popup = new PopupMenu();
        
        
        tIcon = new TrayIcon(CreateIcon("/images/icon.png", "CRYPTO"));
        tIcon.setToolTip("Ödeme Uyarı Sistemi V 1.0");
        final SystemTray fst = SystemTray.getSystemTray(); 
      
        MenuItem guncelList = new MenuItem("Güncel Ödeme Listesi");
        MenuItem belgegir = new MenuItem("Belge Giriş");
        MenuItem ayarlar = new MenuItem("Bildirim saati:"+conf.getProb("saat")+":"+conf.getProb("dakika"));
        MenuItem about = new MenuItem("Hakkında");
        MenuItem cikis = new MenuItem("Çıkış");
      
        popup.add(guncelList);
        popup.add(belgegir);
        popup.add(ayarlar);
        popup.addSeparator();
        popup.add(about);
        popup.add(cikis);
        
     
        tIcon.setPopupMenu(popup);

        cikis.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e){
                System.exit(0);
                
            }
        });
        
        ayarlar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e){
                String saat = JOptionPane.showInputDialog("Lütfen, alarm saatini giriniz: ");
                String dakika = JOptionPane.showInputDialog("Lütfen, alarm dakikasını giriniz: ");
                if (Integer.parseInt(saat) > 23 || Integer.parseInt(dakika) > 59) {
                   
                    JOptionPane.showMessageDialog(null, "Lütfen saat kısmına 24'ten küçük değer giriniz");
                    JOptionPane.showMessageDialog(null, "Lütfen dakika kısmına 59'dan küçük değer giriniz");
                    JOptionPane.showMessageDialog(null, "Lütfen tekrar deneyiniz"); 
                } else {
                        conf.saveProb("saat", saat.trim());
                        conf.saveProb("dakika", dakika.trim());    
                        JOptionPane.showMessageDialog(null, "Değerler kaydedildi. Uyarı saati: "+conf.getProb("saat")+":"+conf.getProb("dakika"));
                        fst.remove(tIcon);
                        showTrayIcon();
                        }    
            }
               
                
               
                
           
        });
        
        guncelList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e){
                guncelList guncelList = new guncelList();
                guncelList.setVisible(true);
                
            }
        });
        
        about.addActionListener(new ActionListener() {
            @Override 
            public void actionPerformed (ActionEvent e){
                JOptionPane.showMessageDialog(null, "Erdoğan Serhat ÇALIŞKAN\ne.serhat.caliskan@gmail.com"); 
                    }
        });
      
      belgegir.addActionListener(new ActionListener() {
            @Override 
            public void actionPerformed (ActionEvent e){
                
                belgeGiris bg = new belgeGiris();
                bg.setVisible(true);
                    }
        });
      try {
            fst.add(tIcon);
            } catch (AWTException e) {
        }
    }
  
         protected static Image CreateIcon (String path, String desc)
    {
        URL ImageURL = trayIcon.class.getResource(path);
        return (new ImageIcon(ImageURL, desc)).getImage();
    }
}
