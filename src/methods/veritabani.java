
package methods;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
        
public class veritabani {
    Connection c = null;
    Statement stmt = null;
    String adres = "jdbc:sqlite:odeme.db";
    
    
  public void yarat(){
          try {
         Class.forName("org.sqlite.JDBC");
         c = DriverManager.getConnection(adres);
        

         stmt = c.createStatement();
         String sql = "CREATE TABLE liste (id INTEGER PRIMARY KEY NOT NULL , bankaAdi STRING NOT NULL, hesapNo STRING NOT NULL, cekNo STRING NOT NULL, tutar STRING NOT NULL, vade STRING NOT NULL, alici STRING NOT NULL, uyariTarihi STRING NOT NULL, odenmeDurum STRING NOT NULL)"; 
         stmt.executeUpdate(sql);
         stmt.close();
         c.close();
         conf.saveProb("vt", "mevcut");
         conf.saveProb("saat", "10");
         conf.saveProb("dakika", "15"); 
         
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage() ); 
      }
     
  
  } 
  
  public void baglan(){
        if("mevcut".equals(conf.getProb("vt"))){
            try {
               
               Class.forName("org.sqlite.JDBC");
               c = DriverManager.getConnection(adres);
               c.setAutoCommit(false);
            
              
        } catch (ClassNotFoundException | SQLException e) {
               JOptionPane.showMessageDialog(null, "Veritabanına bağlanırken sorunla karşılaştık");
            }
    } else {
        yarat();
        }
   }
   public void insert(String sql){
     try {
           
            Statement stmt = c.createStatement();
            stmt.executeUpdate(sql);
            c.commit();
            
        } catch (SQLException ex) {
            Logger.getLogger(veritabani.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getClass().getName() + ": " + ex.getMessage() ); 
        }
   }
   
    public void update(String sql){
     try {
            Statement stmt = c.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(veritabani.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
   
   
  public List<liste> select(String sql){


     try {
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            List<liste> Liste = new ArrayList<>();
          
               while (rs.next()) {
                int id = rs.getInt("id");
                String bankaAdi = rs.getString("bankaAdi");
                String hesapNo = rs.getString("hesapNo");
                String cekNo = rs.getString("cekNo");
                int tutar = rs.getInt("tutar");
                String vade = rs.getString("vade");
                String alici = rs.getString("alici");
                String uyariTarihi = rs.getString("uyariTarihi");
                String odenmeDurum = rs.getString("odenmeDurum");
                
                Liste.add(new liste(id, bankaAdi, hesapNo, cekNo, tutar, vade, alici, uyariTarihi,odenmeDurum));
            }
            
            rs.close();
            stmt.close();
            
            c.close(); 
           return Liste;
            
            } catch (SQLException ex) {
            Logger.getLogger(veritabani.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    
        
         
      
   }
}
