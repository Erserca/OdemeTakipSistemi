
package methods;

import gui.belgeGiris;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import gui.guncelList;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class tableGuncelleme {
   
    Calendar takvim = new GregorianCalendar();
    int ay = takvim.get(Calendar.MONTH)+1;
    int gun = takvim.get(Calendar.DAY_OF_MONTH);
    int yil = takvim.get(Calendar.YEAR);
    String tarih = gun+"/"+ay+"/"+yil;
    
  veritabani vt = new veritabani();  
  String sql = "Select * from liste";
  
  String sqlodenmeyen = "Select * from liste where odenmeDurum = 'Ödenmedi' AND uyariTarihi = '"+tarih+"'";
  
 public void dbListele(){
     
     vt.baglan();
     List<liste> Liste = vt.select(sql);
     DefaultTableModel model = (DefaultTableModel) belgeGiris.vtTablo.getModel();
     model.setRowCount(0);
    
     for (int i = 0; i < Liste.size(); i++) {
         
         Object row[] = {
             Liste.get(i).id,
             Liste.get(i).bankaAdi,
             Liste.get(i).hesapNo,
             Liste.get(i).cekNo,
             Liste.get(i).tutar,
             Liste.get(i).vade,
             Liste.get(i).alici,  
             Liste.get(i).uyariTarihi,
             Liste.get(i).odenmeDurum,
         };
         
         model.addRow(row);
     }
 } 
 
 public void dbListeleParametre(){
     
     vt.baglan();
     List<liste> Liste = vt.select(sqlodenmeyen);
     DefaultTableModel model = (DefaultTableModel) guncelList.vtTablo.getModel();
     model.setRowCount(0);
    
     for (int i = 0; i < Liste.size(); i++) {
         
         Object row[] = {
             Liste.get(i).id,
             Liste.get(i).bankaAdi,
             Liste.get(i).hesapNo,
             Liste.get(i).cekNo,
             Liste.get(i).tutar,
             Liste.get(i).vade,
             Liste.get(i).alici,  
             Liste.get(i).uyariTarihi,
             Liste.get(i).odenmeDurum,
         };
         
         model.addRow(row);
            }
 } 
 public String[] parcala(String tarih){
      
     String []parcala = tarih.split("/");
     return parcala;
  }
 
 
}
