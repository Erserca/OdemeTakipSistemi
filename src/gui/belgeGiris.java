
package gui;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.TableModel;
import methods.tableGuncelleme;
import methods.veritabani;
import methods.temizle;
import methods.vadeHesapla;

public class belgeGiris extends javax.swing.JFrame {

    
    static String vade, uyariVade, odenme;
    public static int id;
    
    tableGuncelleme jts = new tableGuncelleme();
    temizle tm = new temizle();
    
    public belgeGiris() {
        initComponents();
        
        jts.dbListele();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        container = new javax.swing.JPanel();
        bankalbl = new javax.swing.JLabel();
        bankatxt = new javax.swing.JTextField();
        hesapNo = new javax.swing.JTextField();
        cekNo = new javax.swing.JTextField();
        tutari = new javax.swing.JTextField();
        alici = new javax.swing.JTextField();
        uyari = new javax.swing.JLabel();
        kaydet = new javax.swing.JButton();
        guncelle = new javax.swing.JButton();
        ayCb = new javax.swing.JComboBox<>();
        seneCb = new javax.swing.JComboBox<>();
        gunCb = new javax.swing.JComboBox<>();
        sil = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        vtTablo = new javax.swing.JTable();
        hesaolbl = new javax.swing.JLabel();
        vadeGunCb = new javax.swing.JComboBox<>();
        vadeAyCb = new javax.swing.JComboBox<>();
        vadeSeneCb = new javax.swing.JComboBox<>();
        cekNolbl = new javax.swing.JLabel();
        tutarlbl = new javax.swing.JLabel();
        odendimi = new javax.swing.JComboBox<>();
        kalangun = new javax.swing.JLabel();
        vadelbl = new javax.swing.JLabel();
        alicilbl = new javax.swing.JLabel();
        temizle = new javax.swing.JButton();
        kayitlbl = new javax.swing.JLabel();
        bg_lbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);

        container.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        bankalbl.setText("Banka");
        container.add(bankalbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 70, 20));
        container.add(bankatxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 20, 160, 20));
        container.add(hesapNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 50, 160, 20));
        container.add(cekNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 80, 160, 20));
        container.add(tutari, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 20, 160, 20));
        container.add(alici, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 80, 160, 20));

        uyari.setText("Uyarı Tarihi");
        container.add(uyari, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 70, 20));

        kaydet.setText("Kaydet");
        kaydet.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                kaydetMouseClicked(evt);
            }
        });
        container.add(kaydet, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 170, 90, -1));

        guncelle.setText("Güncelle");
        guncelle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guncelleActionPerformed(evt);
            }
        });
        container.add(guncelle, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 170, 90, -1));

        ayCb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ay", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", " " }));
        container.add(ayCb, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 110, 60, 20));

        seneCb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Yıl", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", " " }));
        container.add(seneCb, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 110, -1, 20));

        gunCb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Gün", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        container.add(gunCb, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 110, 60, 20));

        sil.setText("Sil");
        sil.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                silMouseClicked(evt);
            }
        });
        container.add(sil, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 170, 50, -1));

        vtTablo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "Banka", "Hesap No ", "Çek No", "Tutar", "Vade", "Alıcı", "Uyarı Tarihi", "Durum"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        vtTablo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                vtTabloMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(vtTablo);

        container.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 220, 640, 260));

        hesaolbl.setText("Hesap No");
        container.add(hesaolbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 70, 20));

        vadeGunCb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Gün", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        container.add(vadeGunCb, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 50, 60, 20));

        vadeAyCb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ay", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", " " }));
        container.add(vadeAyCb, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 50, 60, 20));

        vadeSeneCb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Yıl", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", " " }));
        container.add(vadeSeneCb, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 50, 60, 20));

        cekNolbl.setText("Çek No");
        container.add(cekNolbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 70, 20));

        tutarlbl.setText("Tutarı");
        container.add(tutarlbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 20, 70, 20));

        odendimi.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ödenme Durumu", "Ödendi", "Ödenmedi" }));
        container.add(odendimi, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 130, 20));
        container.add(kalangun, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 120, 180, 30));

        vadelbl.setText("Vade");
        container.add(vadelbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 50, 70, 20));

        alicilbl.setText("Alıcı");
        container.add(alicilbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 80, 70, 20));

        temizle.setText("Temizle");
        temizle.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                temizleMouseClicked(evt);
            }
        });
        container.add(temizle, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 170, 90, -1));
        container.add(kayitlbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 130, 200, 30));
        container.add(bg_lbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 650, 490));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(container, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(container, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void vtTabloMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_vtTabloMouseClicked
        int i = vtTablo.getSelectedRow();
      
        TableModel model = vtTablo.getModel();
        int id = Integer.parseInt(model.getValueAt(i, 0)+"");
        bankatxt.setText(model.getValueAt(i, 1).toString());
        hesapNo.setText(model.getValueAt(i, 2).toString());
        cekNo.setText(model.getValueAt(i, 3).toString());
        tutari.setText(model.getValueAt(i, 4).toString());
        vade = model.getValueAt(i, 5).toString();
        alici.setText(model.getValueAt(i, 6).toString());
        uyariVade = model.getValueAt(i, 7).toString();
        odenme = model.getValueAt(i, 8).toString();
        odendimi.setSelectedItem(odenme);
        vadeGunCb.setSelectedItem(jts.parcala(vade)[0]);
        vadeAyCb.setSelectedItem(jts.parcala(vade)[1]);
        vadeSeneCb.setSelectedItem(jts.parcala(vade)[2]);
        gunCb.setSelectedItem(jts.parcala(uyariVade)[0]);
        ayCb.setSelectedItem(jts.parcala(uyariVade)[1]);
        seneCb.setSelectedItem(jts.parcala(uyariVade)[2]);
        this.id = id;
        vadeHesapla vh = new vadeHesapla();
        kalangun.setText("Kalan gün sayısı: "+vh.vadeHesapla(vade));
        
    }//GEN-LAST:event_vtTabloMouseClicked

    private void kaydetMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_kaydetMouseClicked
        veritabani vt = new veritabani();
        String vade = vadeGunCb.getSelectedItem().toString()+"/"+vadeAyCb.getSelectedItem().toString()+"/"+vadeSeneCb.getSelectedItem().toString();
        String uyari = gunCb.getSelectedItem().toString()+"/"+ayCb.getSelectedItem().toString()+"/"+seneCb.getSelectedItem().toString();
        odenme = odendimi.getSelectedItem().toString();
        String sql = "insert into 'liste' "
                + "('bankaAdi', 'hesapNo', 'cekNo', 'tutar', 'vade', 'alici', 'uyariTarihi', 'odenmeDurum')values "
                + "('"+bankatxt.getText()+"','"+hesapNo.getText()+"','"+cekNo.getText()+"','"+tutari.getText()+"','"
                +vade+"','"+alici.getText()+"','"+uyari+"','"+odenme+"')";
        vt.baglan();
        vt.insert(sql);
        jts.dbListele();
        tm.temizle();
    }//GEN-LAST:event_kaydetMouseClicked

    private void guncelleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guncelleActionPerformed
        veritabani vt = new veritabani();
        String vade = vadeGunCb.getSelectedItem().toString()+"/"+vadeAyCb.getSelectedItem().toString()+"/"+vadeSeneCb.getSelectedItem().toString();
        String uyari = gunCb.getSelectedItem().toString()+"/"+ayCb.getSelectedItem().toString()+"/"+seneCb.getSelectedItem().toString();
        odenme = odendimi.getSelectedItem().toString();
        String sql = "UPDATE liste set"
                + "'bankaAdi' = '"+bankatxt.getText()+"',"
                + "'hesapNo' = '"+hesapNo.getText()+"',"
                + "'cekNo' = '"+cekNo.getText()+"',"
                + "'tutar' = '"+tutari.getText()+"',"
                + "'vade' = '"+vade+"',"
                + "'alici' = '"+alici.getText()+"',"
                + "'uyariTarihi' = '"+uyari+"',"
                + "'odenmeDurum' = '"+odenme+"' where id = "+id+";";
        vt.baglan();
        vt.insert(sql);
        jts.dbListele();
        tm.temizle();
    }//GEN-LAST:event_guncelleActionPerformed

    private void silMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_silMouseClicked
       veritabani vt = new veritabani();
        String uyari = gunCb.getSelectedItem().toString()+ayCb.getSelectedItem().toString()+seneCb.getSelectedItem().toString();
        String sql = "DELETE from liste where id = "+id+";";
        vt.baglan();
        vt.insert(sql);
        jts.dbListele();
        
    }//GEN-LAST:event_silMouseClicked

    private void temizleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_temizleMouseClicked
     tm.temizle();
    }//GEN-LAST:event_temizleMouseClicked

    public static void main(String args[]) {
    
for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
if ("Windows".equals(info.getName())) {
try {
javax.swing.UIManager.setLookAndFeel(info.getClassName());
} catch (ClassNotFoundException ex) {
Logger.getLogger(belgeGiris.class.getName()).log(Level.SEVERE, null, ex);
} catch (InstantiationException ex) {
                       Logger.getLogger(belgeGiris.class.getName()).log(Level.SEVERE, null, ex);
} catch (IllegalAccessException ex) {
                      Logger.getLogger(belgeGiris.class.getName()).log(Level.SEVERE, null, ex);
                   } catch (UnsupportedLookAndFeelException ex) {
                       Logger.getLogger(belgeGiris.class.getName()).log(Level.SEVERE, null, ex);
                  }
                  break;
            }
}   
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new belgeGiris().setVisible(true);
            }
        });
    
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTextField alici;
    private javax.swing.JLabel alicilbl;
    public static javax.swing.JComboBox<String> ayCb;
    private javax.swing.JLabel bankalbl;
    public static javax.swing.JTextField bankatxt;
    private javax.swing.JLabel bg_lbl;
    public static javax.swing.JTextField cekNo;
    private javax.swing.JLabel cekNolbl;
    private javax.swing.JPanel container;
    public static javax.swing.JComboBox<String> gunCb;
    private javax.swing.JButton guncelle;
    private javax.swing.JLabel hesaolbl;
    public static javax.swing.JTextField hesapNo;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JLabel kalangun;
    private javax.swing.JButton kaydet;
    private javax.swing.JLabel kayitlbl;
    public static javax.swing.JComboBox<String> odendimi;
    public static javax.swing.JComboBox<String> seneCb;
    private javax.swing.JButton sil;
    private javax.swing.JButton temizle;
    public static javax.swing.JTextField tutari;
    private javax.swing.JLabel tutarlbl;
    private javax.swing.JLabel uyari;
    public static javax.swing.JComboBox<String> vadeAyCb;
    public static javax.swing.JComboBox<String> vadeGunCb;
    public static javax.swing.JComboBox<String> vadeSeneCb;
    private javax.swing.JLabel vadelbl;
    public static javax.swing.JTable vtTablo;
    // End of variables declaration//GEN-END:variables
}
