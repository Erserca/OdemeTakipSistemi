package methods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Properties;


public class conf {

    public static Properties prop = new Properties();
    static String dosya = "config.sm";
    
    
    public static void saveProb(String title, String value)
    {
        try { 
                        File file = new File(dosya);
                        FileOutputStream fos = new FileOutputStream(file);
                        OutputStreamWriter osw=new OutputStreamWriter(fos,"UTF-8");
            
            prop.setProperty(title, value);
            prop.store(osw,null);
            
        } catch (IOException e) {
            System.out.println("Exception: "+e);
        }
    }  
       
        public static void saveProb(String title, int value)
    {
        try { 
                        File file = new File(dosya);
                        FileOutputStream fos = new FileOutputStream(file);
                        OutputStreamWriter osw=new OutputStreamWriter(fos,"UTF-8");
            
            prop.setProperty(title, value+"");
            prop.store(osw,null);
            
        } catch (IOException e) {
            System.out.println("Exception: "+e);
        }
    }  
       
    
    public static String getProb(String title)
    {
      String value = "";
        try { 
           
            prop.load(new FileInputStream(dosya));
            value = prop.getProperty(title);
            
        } catch (IOException e) {
            System.out.println("Exception: "+e);
        }
    return value;
}
}

